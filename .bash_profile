# ~/.bash_profile: executed by the command interpreter for login shells

[ -f "$HOME/.bashrc" ] && . $HOME/.bashrc

# Styling QT apps
export QT_QPA_PLATFORMTHEME=gtk2

# Set PATH so it includes user's private bins if they exist
[ -d "$HOME/bin" ] && export PATH="$HOME/bin:$PATH"
[ -d "$HOME/.local/bin" ] && export PATH="$HOME/.local/bin:$PATH"

# Manage multiple versions of Python using pyenv
if [ -d "$HOME/.pyenv" ] ; then
    export PYENV_ROOT="$HOME/.pyenv"
    command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"
    eval "$(pyenv init -)"
fi
