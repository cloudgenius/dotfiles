" ----------------
" General settings
" ----------------

filetype plugin indent on
set nocompatible
set number
set cc=100
set mouse=
set ignorecase
set smartcase
set showmatch
set hlsearch
set incsearch
set autoindent
set expandtab
set tabstop=2
set shiftwidth=2
set softtabstop=2
set wildmode=longest,list
set cmdwinheight=10
set cursorline
set cursorlineopt=number
set statusline=%<%n\ %t%m%r\ \ %a\ \ %=L%l.\C%c%V.\T%L\ %P

" -------
" Colours
" -------

syntax on
colorscheme nord

" -------------
" Mode mappings
" -------------

" normal mode map = `:nnoremap`
" insert mode map = `:inoremap`

let g:mapleader = ','

nnoremap <leader>s :set invspell<CR>
nnoremap <leader>ed :read ~/.config/nvim/_misc/everyday.txt<CR>
inoremap <leader>d <C-R>=strftime("%Y-%m-%dT%H:%M")<CR>

" -------
" Plugins
" -------

call plug#begin('~/.config/nvim/plugged')

Plug 'Raimondi/delimitMate'
Plug 'arcticicestudio/nord-vim'
Plug 'godlygeek/tabular'
Plug 'preservim/vim-markdown'
Plug 'vimwiki/vimwiki'

call plug#end()

" ------
" Extras
" ------

let g:vimwiki_list = [{'path': '~/doc/wiki/', 'path_html': '~/doc/wiki/html/'}]
let g:vimwiki_url_maxsave=0
